from django.shortcuts import render

# Create your views here.
def riwayat(request):
    context = {
        'page' : 'riwayat',
        'title' : 'Riwayat',
        'subtitle1' : 'Lupa dulu pernah makan apa?',
        'subtitle2' : 'Cek aja di sini!',
        
    }

    return render(request, 'riwayat.html', context)
