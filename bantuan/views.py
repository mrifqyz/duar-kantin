from django.shortcuts import render, redirect

from .forms import BantuanForm
from .models import Bantuan

# Create your views here.
def bantuan(request):
    form = BantuanForm();
    bantuan_data = Bantuan.objects.all()

    if(request.method == "POST"):
        form = BantuanForm(request.POST)
        if form.is_valid():
            Bantuan.objects.create(
                pertanyaan = request.POST['pertanyaan'],
                jawaban = 'Belum Dijawab'
            )    
        return redirect('/bantuan')

    forms = {
        'form':form,
        'data': bantuan_data
    }

    context = {
        'page' : 'bantuan',
        'title' : 'Bantuan',
        'subtitle1' : 'Penasaran dengan website ini?',
        'subtitle2' : 'Atau bingung cara menggunakannya?',
        'form' : forms
    }

    return render(request, 'bantuan.html', context)
