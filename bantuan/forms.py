from django import forms

from .models import Bantuan

class BantuanForm(forms.Form):
    pertanyaan = forms.CharField(
    label='Pertanyaan', 
    max_length=100, 
    widget=forms.TextInput(attrs={
            'class':'form-style'
        }), 
);

    email = forms.EmailField(
    label='E-mail',  
    widget=forms.EmailInput(attrs={
            'class':'form-style'
        }), 
);