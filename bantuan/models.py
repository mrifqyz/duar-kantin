from django.db import models

# Create your models here.
class Bantuan(models.Model):
    pertanyaan = models.CharField(max_length=100);
    jawaban = models.TextField();

    def __str__(self):
        return self.pertanyaan;