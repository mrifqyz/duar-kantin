from django.test import TestCase, Client

# Create your tests here.

class OrderPageTest(TestCase):
    def test_open_order_page(self):
        response = Client().get('/order/')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_pesanan_word(self):
        response = Client().get('/order/')
        self.assertContains(response, 'Pesanan')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_detail_pembayaran_word(self):
        response = Client().get('/order/')
        self.assertContains(response, 'Detail Pembayaran')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_bayar_menggunakan_word(self):
        response = Client().get('/order/')
        self.assertContains(response, 'Bayar Menggunakan')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_harga_word(self):
        response = Client().get('/order/')
        self.assertContains(response, 'Harga')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_cash_word(self):
        response = Client().get('/order/')
        self.assertContains(response, 'Cash')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_gopay_image(self):
        response = Client().get('/order/')
        self.assertContains(response, 'go-pay.svg')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_tambah_pesanan_word(self):
        response = Client().get('/order/')
        self.assertContains(response, '+Tambah Pesanan')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_buat_pesanan_button(self):
        response = Client().get('/order/')
        self.assertContains(response, '<input class="btn')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_catatan_form(self):
        response = Client().get('/order/')
        self.assertContains(response, '<input type="text"')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_radio_button(self):
        response = Client().get('/order/')
        self.assertContains(
            response, '<input type="radio"')
        self.assertEqual(response.status_code, 200)

    def test_contains_navbar(self):
        response = Client().get('/order/')
        self.assertContains(response, '<nav')
        self.assertEqual(response.status_code, 200)

    def test_contains_logo(self):
        response = Client().get('/order/')
        self.assertContains(response, 'logo.png')
        self.assertEqual(response.status_code, 200)

    def test_contains_bootstrap(self):
        response = Client().get('/order/')
        self.assertContains(response, '''<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">''')
        self.assertEqual(response.status_code, 200)

    def test_contains_custom_css(self):
        response = Client().get('/order/')
        self.assertContains(response, '''<link rel="stylesheet" href="/static/order.css">''')
        self.assertEqual(response.status_code, 200)

    def test_contains_footer(self):
        response = Client().get('/order/')
        self.assertContains(response, '</footer>')
        self.assertEqual(response.status_code, 200)

    def test_pages_loaded_google_font_PTSans(self):
        response = Client().get('/order/')
        self.assertContains(response, '<link href="https://fonts.googleapis.com/css?family=PT+Sans:700,700i&display=swap" rel="stylesheet">')
        self.assertEqual(response.status_code, 200)

    def test_pages_loaded_google_font_Poppins(self):
        response = Client().get('/order/')
        self.assertContains(response, '<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,700&display=swap" rel="stylesheet">')
        self.assertEqual(response.status_code, 200)

    def test_pages_load_jquery(self):
        response = Client().get('/order/')
        self.assertContains(response, ''' <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>''')
        self.assertEqual(response.status_code, 200)

    def test_pages_load_popper_js(self):
        response = Client().get('/order/')
        self.assertContains(response, '''<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>''')
        self.assertEqual(response.status_code, 200)

    def test_pages_head_is_kane(self):
        response = Client().get('/order/')
        self.assertContains(response, '<title>kane.</title>')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_footer_text(self):
        response = Client().get('/order/')
        self.assertContains(response, '©2019 kane (KAntin oNlinE), All rights reserved.')
        self.assertEqual(response.status_code, 200)