from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.
class Pemesanan(models.Model):
    
    waktu = models.TimeField()
    total_harga = models.IntegerField()
    items = models.TextField()
    method = models.CharField(max_length=20)
    