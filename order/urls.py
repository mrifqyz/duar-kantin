from django.urls import re_path
from .views import order, make_order

urlpatterns = [
	re_path(r'^$', order, name='order'),
	re_path(r'^make_order', make_order, name='make_order'),
]