from django.urls import re_path, path
from .views import *

urlpatterns = [
	path("", index, name='index_canteen'),
	re_path(r'^add_canteen_form', add_canteen_form, name='add_canteen_form'),
	re_path(r'^add_canteen', add_canteen, name='add_canteen'),
	re_path(r'^add_menu_form', add_menu_form, name='add_menu_form'),
	path('food', detail_of_food, name='makanan'),
	re_path(r'^(?P<kantin>[\w-]+)', detail_kantin, name='detail_kantin' ),
]
