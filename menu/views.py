from django.shortcuts import render, redirect
from django.urls import reverse
from .models import Canteen, Food
from .forms import CanteenForm, FoodForm

# Create your views here.


def index(request):
    context = {
        'page': 'kantin',
        'title': 'Pilihan Kantin',
        'subtitle1': 'Mau makan apa dan di mana?',
        'subtitle2': 'Pilih aja langsung kantinnya!',
        'canteens': Canteen.objects.all()
    }
    return render(request, 'index.html', context)


def add_canteen_form(request):
    canteen_form = CanteenForm()
    context = {
        'page': 'kantin',
        'title': 'Tambah Kantin',
        'subtitle1': 'Kantin favoritmu belum ada di sini?',
        'subtitle2': 'Yuk tambahkan!',
        'form': canteen_form
    }
    return render(request, 'canteenform.html', context)


def add_canteen(request):
    if (request.method == "POST"):
        CanteenForm(request.POST).save()
    return redirect(reverse('index_canteen'))


def detail_of_food(request):
    return render(request, "food-desc.html")


def add_menu_form(request):
    menu_form = FoodForm()
    if request.method == 'POST':
        parsed_form = FoodForm(request.POST)
        if parsed_form.is_valid():
            kantin = request.POST['kantin']
            try:
                kantin2 = list(Canteen.objects.filter(nama=kantin))
                Food.objects.create(nama=request.POST['nama'], harga=request.POST['harga'], gambar=request.POST['gambar'],
                                    deskripsi=request.POST['deskripsi'], stok=request.POST['stok'], kantin=kantin2[0])
                return redirect('/menu')
            except:
                return redirect('/menu')

    context = {
        'page': 'kantin',
        'title': 'Tambah Menu',
        'subtitle1': 'Menu baru, makanan baru, makin',
        'subtitle2': 'banyak variasi deh!',
        'form': menu_form
    }
    return render(request, 'menuform.html', context)

    
def detail_kantin(request, kantin):
    canteen = list(Canteen.objects.filter(slug=kantin))
    list_food = Food.objects.filter(kantin=canteen[0])
    data = {
            'page': 'kantin',
            'title': 'Kantin ' + canteen[0].nama,
                    'subtitle1': 'Yuk pilih mau makan apa!',
            'menu': list_food,
        }

    return render(request, 'canteen-detail.html', data)


