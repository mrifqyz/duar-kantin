from django.db import models
from django_extensions.db.fields import AutoSlugField
# Create your models here.

class Canteen(models.Model):
    nama = models.CharField(max_length=30000, blank=False)
    jam_buka = models.TimeField(blank=False)
    jam_tutup = models.TimeField(blank=False)
    kategori = models.CharField(max_length=30000, blank=False)
    gambar = models.CharField(max_length=30000, blank=False)
    slug = AutoSlugField(populate_from='nama')

    def __str__(self):
        return self.nama

    class Meta:
        ordering = ['nama']

class Food(models.Model):
    nama = models.CharField(max_length=30000, blank=False)
    harga = models.PositiveIntegerField(blank=False)
    gambar = models.FileField(upload_to='uploads/')
    deskripsi = models.CharField(max_length=30000, blank=False)
    stok = models.PositiveIntegerField(blank=False)
    slug = AutoSlugField(populate_from='nama')
    kantin = models.ForeignKey(Canteen, on_delete=models.CASCADE, default=1,null=True)

    def __str__(self):
        return self.nama

    class Meta:
        ordering = ['nama']

