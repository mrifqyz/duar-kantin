from django.forms import ModelForm
from django import forms
from .models import Canteen, Food


class CanteenForm(ModelForm):
    class Meta:
        model = Canteen
        fields = ['nama', 'jam_buka', 'jam_tutup', 'kategori', 'gambar']
        widgets = {
            'nama': forms.TextInput(attrs={
                'class': 'form-control'}),

            'jam_buka': forms.TimeInput(attrs={
                'class': 'form-control',
                'type': 'time'}),

            'jam_tutup': forms.TimeInput(attrs={
                'class': 'form-control',
                'type': 'time'}),

            'kategori': forms.TextInput(attrs={
                'class': 'form-control'}),

            'gambar': forms.TextInput(attrs={
                'class': 'form-control'})
        }


class FoodForm(forms.Form):

    kantin = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    nama = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    harga = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control'}))
    gambar = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    deskripsi = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    stok = forms.IntegerField(widget=forms.NumberInput(
        attrs={'class': 'form-control'}))